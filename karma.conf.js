// setup via https://medium.com/@grrowl/testing-react-with-jasmine-and-karma-using-webpack-and-babel-18fc268f066a
// read more in docs http://karma-runner.github.io/2.0/config/configuration-file.html

process.env.NODE_ENV = "development";

// this should be the same config used to build the app - we can reuse the one provided by create-react-app
const webpackConfig = require("./node_modules/react-scripts/config/webpack.config.dev.js");

module.exports = function(config) {
    config.set({

        browsers: ["Chrome", "Firefox"],        // Add any browsers here, for example ChromeHeadless
        frameworks: ["jasmine"],

        basePath: "./",                         // The entry point for our test suite
        autoWatch: true,
        files: ["karma.entrypoint.js"],
        preprocessors: {
            "karma.entrypoint.js": ["webpack", "sourcemap"],    // Run through webpack, and enable inline sourcemaps
        },

        webpack: webpackConfig,
        client: {
            captureConsole: true                // log console output in our test console
        },

        reporters: ["progress"],
        singleRun: false,                       // watch for changes and automatically re-run tests

        webpackMiddleware: {
            noInfo: true
        },

        // Webpack takes a little while to compile -- this manifests as a really
        // long load time while webpack blocks on serving the request.
        browserNoActivityTimeout: 60000, // 60 seconds

    });
};
