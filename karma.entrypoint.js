/**
 * Provides single entrypoint for karma-webpack to load all tests dynamically based on filename.
 */

// we need to setup enzyme
require("./src/setupTests");

// include all spec files
const context = require.context("./src", true, /\.spec\.js/);
context.keys().forEach(context);
