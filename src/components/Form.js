import React from "react";
import {connect} from "react-redux";
import {getFormPriority, getFormText} from "../ducks/form";


export const FormPresenter = ({priority, text, onChangePriority, onChangeText, onSave, onCancel}) => {

    const onSubmit = (e) => {
        e.preventDefault();
        onSave();
    };

    return (
        <form onSubmit={onSubmit} className="todo-form">
            <input type="text"
                   value={text}
                   onChange={e => onChangeText(e.currentTarget.value)}
                   autoFocus={true}
            />
            <select value={priority} onChange={e => onChangePriority(e.currentTarget.value)}>
                <option value="high">High</option>
                <option value="normal">Normal</option>
                <option value="low">Low</option>
            </select>
            <button className="save" >save</button>
            <button type="button" className="cancel" onClick={onCancel}>cancel</button>
        </form>
    );
};

export const Form = connect(
    // in this case map-to-state method is not extracted and therefore not testable
    (state) => ({
        text: getFormText(state),
        priority: getFormPriority(state)
    }),
    (dispatch) => ({
        onChangeText: (payload) => dispatch({type: "SET_TEXT", payload}),
        onChangePriority: (payload) => dispatch({type: "SET_PRIORITY", payload}),
        onSave: (payload) => dispatch({type: "SAVE", payload}),
        onCancel: () => dispatch({type: "CANCEL"}),
    })
)(FormPresenter);
