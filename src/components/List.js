import React from "react";
import {connect} from "react-redux";
import {Form} from "./Form";
import {getList} from "../ducks/list";
import {getEditedItem} from "../ducks/form";


export const ListItem = ({id, text, priority, doEdit, doDelete}) => {

    return (
        <div className={`item item--${priority}`}>
            <span className="id">#{id}</span>
            <span className="text">{text}</span>
            <span className="options">
                <button className="edit" onClick={() => doEdit(id)}>edit</button>
                <button className="delete" onClick={() => doDelete(id)}>delete</button>
            </span>
        </div>
    );
};

export const ListPresenter = ({list, editedItem, doEdit, doDelete}) => (
    <ul className="todo-list">
        {list.map(item => (
            <li key={item.id}>
                {editedItem === item.id ? <Form /> : <ListItem {...item} doEdit={doEdit} doDelete={doDelete}/>}
            </li>
        ))}
    </ul>
);

export const List = connect(
    (state) => ({
        list: getList(state),
        editedItem: getEditedItem(state)
    }),
    (dispatch) => ({
        doEdit: (payload) => dispatch({type: "EDIT", payload}),
        doDelete: (payload) => dispatch({type: "DELETE", payload})
    })
)(ListPresenter);
