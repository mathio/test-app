import React from "react";
import {shallow} from "enzyme/build/index";
import {FormPresenter} from "./Form";
import {getCreateSpy} from "../spec/testUtils";

// see App.spec.js for more extensive comments
describe("Form.js", () => {

    describe("<FormPresenter/>", () => {
        const props = {
            priority: "high",
            text: "foo",
            onChangePriority: getCreateSpy()(),
            onChangeText: getCreateSpy()(),
            onSave: getCreateSpy()(),
            onCancel: getCreateSpy()()
        };
        const wrapper = shallow(<FormPresenter {...props} />);

        it("should render form", () => {
            expect(wrapper.find("form").length).toBe(1);
        });

        describe("text input", () => {
            const input = wrapper.find("form > input");

            it("should be rendered with correct value", () => {
                expect(input.prop("value")).toBe("foo");
            });

            describe("when value is changed", () => {
                // when simulating an event we need to pass the value
                input.simulate("change", {currentTarget: {value: "bar"}});

                it("should dispatch correct action with payload", () => {
                    expect(props.onChangeText).toHaveBeenCalledWith("bar");
                });
            });
        });

        describe("priority select", () => {
            const select = wrapper.find("form > select");

            it("should be rendered with correct value", () => {
                expect(select.prop("value")).toBe("high");
            });

            describe("options", () => {
                const options = select.find("option");

                it("should render correct values", () => {
                    expect(options.map(option => option.prop("value"))).toEqual(["high", "normal", "low"]);
                });

                it("should render correct texts", () => {
                    expect(options.map(option => option.text())).toEqual(["High", "Normal", "Low"]);
                });
            });

            describe("when value is changed", () => {
                // when simulating an event we need to pass the value
                select.simulate("change", {currentTarget: {value: "low"}});

                it("should dispatch correct action with payload", () => {
                    expect(props.onChangePriority).toHaveBeenCalledWith("low");
                });
            });
        });

        describe("save button", () => {
            const button = wrapper.find("form > button.save");

            it("should be rendered with correct value", () => {
                expect(button.text()).toBe("save");
            });
        });

        describe("cancel button", () => {
            const button = wrapper.find("form > button.cancel");

            it("should be rendered with correct value", () => {
                expect(button.text()).toBe("cancel");
            });

            describe("when it is clicked", () => {
                // when simulating an event we need to pass the value
                button.simulate("click");

                it("should dispatch correct action", () => {
                    expect(props.onCancel).toHaveBeenCalled();
                });
            });
        });

        describe("when the form is submitted", () => {
            // when simulating an event we need to pass the value
            const event = {
                preventDefault: getCreateSpy()()
            };
            wrapper.find("form").simulate("submit", event);

            it("should call prevent default event action", () => {
                expect(event.preventDefault).toHaveBeenCalled();
            });

            it("should dispatch correct action", () => {
                expect(props.onSave).toHaveBeenCalled();
            });
        });

    });
});
