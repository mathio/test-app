import React from "react";
import {AppPresenter, mapStateToAppProps} from "./App";
import {shallow} from "enzyme";
import {Form} from "./Form";
import {List} from "./List";
import {getCreateSpy, getSpyOn} from "../spec/testUtils";

// wrap all tests in a file with filename to better identify failed tests in logs
describe("App.js", () => {

    // components should be described using JSX syntax
    describe("<AppPresenter/>", () => {
        let consoleSpy;
        let wrapper;

        // define props into separate variable to reuse it later
        // when using typescript you can type the variable here to make sure you are passing correct props
        const props = {
            showForm: false,
            addItem: getCreateSpy()()    // placeholder method to test is it was called
        };

        beforeAll(() => {
            // spies need to be created inside beforeAll()
            // first arg is object, second is method
            // to test custom method without object you can import it using `import * as obj from "./file";`
            consoleSpy = getSpyOn()(console, "log");

            // normally this can be defined outside beforeAll(), but we need to register spy first
            // spy can be registered only inside beforeAll() and component must be rendered after the spy is registered
            wrapper = shallow(<AppPresenter {...props} />);
        });

        // as a rule of thumb text in "it" should almost always start with "should"
        it("should render with correct class name", () => {
            expect(wrapper.hasClass("app")).toBe(true);
        });

        it("should render heading", () => {
            expect(wrapper.find("div.app > h1").text()).toBe("Todo List");
        });

        it("should render <List/>", () => {
            expect(wrapper.find(List).length).toBe(1);
        });

        it("should not render <Form/>", () => {
            expect(wrapper.find(Form).length).toBe(0);
        });

        it("should log to console", () => {
            expect(consoleSpy).toHaveBeenCalledWith("Application ready");
        });

        describe("the link to add new item", () => {
            // define inside describe to reuse in tests
            let addLink;

            beforeAll(() => {
                // this needs to be inside beforeAll() because wrapper was assigned inside it as well
                addLink = wrapper.find("a");
            });

            it("should be rendered", () => {
                expect(addLink.text()).toBe("add new item");
            });

            describe("when the link to add new item is clicked", () => {

                beforeAll(() => {
                    // the event simulation belongs inside "describe" not "it" block (if there is one)
                    addLink.simulate("click");
                });

                it("should dispatch action to show form", () => {
                    // we confirm correct prop was called
                    expect(props.addItem).toHaveBeenCalled();
                });
            });
        });

        describe("when the form is displayed", () => {
            // reuse props here - in this case its just 2 props, but when there are are many this will make the code
            // easier to read, because we see exactly what was overwritten for this test case
            const wrapperWithForm = shallow(<AppPresenter {...{...props, showForm: true}} />);

            it("should not render link to add new item", () => {
                expect(wrapperWithForm.find("a").length).toBe(0);
            });

            it("should render <Form/>", () => {
                expect(wrapperWithForm.find(Form).length).toBe(1);
            });
        });
    });

    // method names should start with #
    describe("#mapStateToAppProps", () => {

        // helper method to reuse the (partial) state and only change what is important for the selector
        // I think it is acceptable to use partial state (and in typescript cast it as any to silence linter)
        const mockState = (id) => ({
            form: {
                id,
                text: "foo",
                priority: "high"
            }
        });

        describe("when id of edited item is zero", () => {
            // place state for this scenario inside describe to reuse in each it
            const state = mockState(0);

            it("should return true", () => {
                // when comparing objects use toEqual for deep comparison
                expect(mapStateToAppProps(state)).toEqual({showForm: true});
            });
        });

        describe("when id of edited item is not zero", () => {
            const state = mockState(1);

            it("should return false", () => {
                expect(mapStateToAppProps(state)).toEqual({showForm: false});
            });
        });
    });
});
