import React from "react";
import {shallow} from "enzyme/build/index";
import {ListItem, ListPresenter} from "./List";
import {Form} from "./Form";
import {getCreateSpy} from "../spec/testUtils";

// see App.spec.js for more extensive comments
describe("List.js", () => {

    describe("<ListPresenter/>", () => {
        const list = [
            {id: 1, text: "milk", priority: "high"},
            {id: 2, text: "bread", priority: "normal"},
            {id: 3, text: "chocolate", priority: "low"}
        ];
        const doEdit = getCreateSpy()();
        const doDelete = getCreateSpy()();

        const props = {
            list,
            editedItem: 0,
            doEdit,
            doDelete
        };
        const wrapper = shallow(<ListPresenter {...props} />);

        it("should render list", () => {
            expect(wrapper.find("ul").length).toBe(1);
        });

        it("should render 3 list items", () => {
            expect(wrapper.find("ul > li").length).toBe(3);
        });

        it("should render 3 <ListItem/> with correct props", () => {
            expect(wrapper.find(ListItem).map(item => item.props()))
                .toEqual(list.map(item => ({...item, doEdit, doDelete})));
        });

        describe("when item is being edited", () => {
            const editedWrapper = shallow(<ListPresenter {...{...props, editedItem: 2}} />);

            it("should render 3 list items", () => {
                expect(editedWrapper.find("ul > li").length).toBe(3);
            });

            it("should render 2 <ListItem/>", () => {
                expect(editedWrapper.find(ListItem).length).toBe(2);
            });

            it("should render 1 <Form/>", () => {
                expect(editedWrapper.find(Form).length).toBe(1);
            });
        });
    });
});
