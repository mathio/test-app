import React from "react";
import {List} from "./List";
import {Form} from "./Form";
import {connect} from "react-redux";
import {showNewItemForm} from "../ducks/form";


export class AppPresenter extends React.Component {

    componentDidMount() {
        console.log("Application ready");
    }

    render() {
        const {showForm, addItem} = this.props;

        return (
            <div className="app">
                <h1>Todo List</h1>
                <List/>
                {showForm ? <Form/> : <a className="add-item" onClick={addItem}>add new item</a>}
            </div>
        );
    }
};

// if you really want to test this, you can extract this as selector to make it testable
// I think this is a bit too much, since all selectors used here are already tested
export const mapStateToAppProps = (state) => ({
    showForm: showNewItemForm(state)
});

export const App = connect(
    mapStateToAppProps,
    (dispatch) => ({
        addItem: () => dispatch({type: "ADD_ITEM"})
    })
)(AppPresenter);
