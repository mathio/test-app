import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import {App} from "./components/App";
import {Provider} from "react-redux";
import {applyMiddleware, createStore} from "redux";
import createSagaMiddleware from "redux-saga";
import {appReducer} from "./store/reducer";
import {appSaga} from "./store/saga";

const sagaMiddleware = createSagaMiddleware();

const theStore = createStore(appReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(appSaga);

export const Client = () => (
    <Provider store={theStore}>
        <App/>
    </Provider>
);

const rootElm = document.getElementById("root");

if (rootElm) {
    ReactDOM.render(<Client/>, rootElm);
}
