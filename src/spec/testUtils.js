import deepFreeze from "deep-freeze";


/**
 * Test utility to unwrap the inner method of the reselect library selector.
 *
 * Allows calling the inner method directly with its parameters instead of passing the whole state.
 * Removes dependency on partial selectors when testing.
 *
 * This should not be used outside of unit tests.
 */
export const unwrapSelector = (selector) => selector.resultFunc;


/**
 * Test utility that deep-freezes inputs to the reducer. Since reducers must be pure and
 * should not modify state nor action, this utility to catches any accidental mutations.
 *
 * Return value is a reducer with the same signature as the original reducer.
 *
 * You should wrap your reducer before testing.
 *
 * This should not be used outside of unit tests.
 */
export const wrapReducerForTesting = (originalReducer) => {
    return (state, action) => {
        if (state) {
            deepFreeze(state);
        }
        if (action) {
            deepFreeze(action);
        }
        return originalReducer(state, action);
    };
};

/**
 * Helper methods just for this demo. Normally you pick your test runner and use its syntax.
 * In this demo we can run tests using both jest or jasmine.
 *
 * When running via jasmine the code does not have access to env variables because the code is actually
 * running on client (inside the browser). However when running via jest we can access env variables.
 */
const useJest = process.env.USE_JEST === "true";
export const getSpyOn = () => useJest ? jest.spyOn : spyOn;
export const getCreateSpy = () => useJest ? jest.fn : jasmine.createSpy;
