import * as Effects from "redux-saga/effects"
import {saveFormSaga} from "../ducks/form";
import {editItemSaga} from "../ducks/list";

export function* appSaga() {
    yield Effects.takeEvery("SAVE", saveFormSaga);
    yield Effects.takeEvery("EDIT", editItemSaga);
}