import {combineReducers} from "redux";
import {listReducer} from "../ducks/list";
import {formReducer} from "../ducks/form"


export const appReducer = combineReducers({
    list: listReducer,
    form: formReducer
});
