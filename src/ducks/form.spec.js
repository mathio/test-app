import {getCreateSpy, unwrapSelector, wrapReducerForTesting} from "../spec/testUtils";
import {formReducer, getEditedItem, getForm, getFormPriority, getFormText, saveFormSaga, showNewItemForm} from "./form";
import {runSaga} from "redux-saga";

// see App.spec.js for more extensive comments
describe("form.js duck", () => {

    // reducer is a method and method names should start with #
    describe("#formReducer", () => {
        const reducer = wrapReducerForTesting(formReducer);
        const addingState = {
            id: 0,
            text: "",
            priority: "normal"
        };
        const editingState = {
            id: 1,
            text: "foo",
            priority: "high"
        };

        // just list the action type
        describe("ADD_ITEM", () => {

            describe("when no item is being edited", () => {
                const emptyState = {};

                it("should populate form with empty item", () => {
                    expect(reducer(emptyState, {type: "ADD_ITEM"})).toEqual({
                        id: 0,
                        text: "",
                        priority: "normal"
                    });
                });
            });

            describe("when there is an item being edited", () => {

                it("should not populate form with empty item", () => {
                    // here we SHOULD use "toBe" because reducer should not change the object and return the same one
                    expect(reducer(addingState, {type: "ADD_ITEM"})).toBe(addingState);
                });
            });
        });

        describe("START_EDITING", () => {
            const payload = {
                id: 3,
                text: "foo",
                priority: "low"
            };

            describe("when no item is being edited", () => {
                const emptyState = {};

                it("should populate form with empty item", () => {
                    expect(reducer(emptyState, {type: "START_EDITING", payload})).toEqual(payload);
                });
            });

            describe("when there is an item being added", () => {

                it("should not populate form with empty item", () => {
                    // here we SHOULD use "toBe" because reducer should not change the object and return the same one
                    expect(reducer(addingState, {type: "START_EDITING", payload})).toBe(addingState);
                });
            });

            describe("when there is another item being edited", () => {

                it("should not populate form with empty item", () => {
                    // here we SHOULD use "toBe" because reducer should not change the object and return the same one
                    expect(reducer(editingState, {type: "START_EDITING", payload})).toBe(editingState);
                });
            });
        });

        describe("SET_TEXT", () => {

            it("should change text", () => {
                expect(reducer(editingState, {type: "SET_TEXT", payload: "bar"})).toEqual({...editingState, text: "bar"});
            });
        });

        describe("SET_PRIORITY", () => {

            it("should change priority", () => {
                expect(reducer(editingState, {type: "SET_PRIORITY", payload: "low"})).toEqual({...editingState, priority: "low"});
            });
        });

        describe("CANCEL", () => {

            it("should clear the state", () => {
                expect(reducer(editingState, {type: "CANCEL"})).toEqual({});
            });
        });

        describe("SAVED", () => {

            it("should clear the state", () => {
                expect(reducer(editingState, {type: "SAVED"})).toEqual({});
            });
        });

        describe("OTHER_ACTION", () => {

            it("should not change state", () => {
                expect(reducer(editingState, {type: "OTHER_ACTION"})).toBe(editingState);
            });
        });
    });

    describe("selectors", () => {
        const form = {
            id: 4,
            text: "foo",
            priority: "normal"
        };

        describe("#getForm", () => {
            const state = {
                form
            };

            it("should return form from the state", () => {
                expect(getForm(state)).toBe(form);
            });
        });

        describe("#getFormText", () => {
            const selector = unwrapSelector(getFormText);

            it("should return form text", () => {
                expect(selector(form)).toBe("foo");
            });
        });

        describe("#getFormPriority", () => {
            const selector = unwrapSelector(getFormPriority);

            it("should return form priority", () => {
                expect(selector(form)).toBe("normal");
            });
        });

        describe("#getEditedItem", () => {
            const selector = unwrapSelector(getEditedItem);

            it("should return form id", () => {
                expect(selector(form)).toBe(4);
            });
        });

        describe("#showNewItemForm", () => {
            const selector = unwrapSelector(showNewItemForm);

            describe("when edited item id is zero", () => {

                it("should return true", () => {
                    expect(selector(0)).toBe(true);
                });
            });

            describe("when edited item id is not zero", () => {

                it("should return false", () => {
                    expect(selector(9)).toBe(false);
                });
            });
        });
    });

    describe("#saveFormSaga", async () => {
        const dispatch = jest.fn();

        await runSaga({
            dispatch,
            getState: () => ({ form: "foobar" }),
        }, saveFormSaga).done;

        it("should dispatch SAVED action with current form", () => {
            expect(dispatch).toHaveBeenCalledWith({type: "SAVED", payload: "foobar"});
        });
    });
});
