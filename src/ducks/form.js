import * as Effects from "redux-saga/effects";
import {createSelector} from "reselect";


const emptyState = {
    id: 0,
    text: "",
    priority: "normal"
};

// this is not exported because it should not be tested directly - we test this through the reducer
const populateFormIfPossible = (state, data) => state.id === undefined ? {...data} : state;

export const formReducer = (state = {}, {type, payload}) => {
    switch (type) {
        case "ADD_ITEM":
            return populateFormIfPossible(state, emptyState);
        case "START_EDITING":
            return populateFormIfPossible(state, payload);
        case "SET_TEXT":
            return {...state, text: payload};
        case "SET_PRIORITY":
            return {...state, priority: payload};
        case "CANCEL":
        case "SAVED":
            return {};
        default:
            return state;
    }
};

export const getForm = (state) => state.form;

export const getFormText = createSelector(getForm, form => form.text);

export const getFormPriority = createSelector(getForm, form => form.priority);

export const getEditedItem = createSelector(getForm, form => form.id);

export const showNewItemForm = createSelector(getEditedItem, editedItem => editedItem === 0);


export function* saveFormSaga() {
    const newItem = yield Effects.select(getForm);

    // TODO: api request

    yield Effects.put({type: "SAVED", payload: newItem});
}
