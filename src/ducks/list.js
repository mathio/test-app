import * as Effects from "redux-saga/effects";


const defaultState = [
    {id: 1, text: "milk", priority: "high"},
    {id: 2, text: "bread", priority: "normal"},
    {id: 3, text: "eggs", priority: "normal"},
    {id: 4, text: "chocolate", priority: "low"}
];

// this is not exported because it should not be tested directly - we test this through the reducer
const onSave = (state, payload) => {
    if (payload.id > 0) {
        return state.map(item => item.id === payload.id ? payload : item);
    } else {
        const id = Math.max(0, state.map(({id}) => id).sort().reverse()[0]) + 1;
        return [...state, {...payload, id}];
    }
};

// this is not exported because it should not be tested directly - we test this through the reducer
const onDelete = (state, payload) => state.filter(({id}) => id !== payload)

export const listReducer = (state = defaultState, {type, payload}) => {
    switch (type) {
        case "SAVED":
            return onSave(state, payload);
        case "DELETE":
            return onDelete(state, payload);
        default:
            return state;
    }
};

export const getList = (state) => state.list;

export const getListItemById = (state, id) => getList(state).find(item => item.id === id);


export function* editItemSaga({payload}) {
    const itemToEdit = yield Effects.select(getListItemById, payload);
    yield Effects.put({type: "START_EDITING", payload: itemToEdit});
}