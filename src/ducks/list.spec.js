import {editItemSaga, getList, getListItemById, listReducer} from "./list";
import {getCreateSpy, wrapReducerForTesting} from "../spec/testUtils";
import {runSaga} from "redux-saga";

// see App.spec.js for more extensive comments
describe("list.js duck", () => {
    const state = [
        {id: 1, text: "milk", priority: "high"},
        {id: 2, text: "bread", priority: "normal"},
        {id: 3, text: "egggs", priority: "normal"}
    ];

    // reducer is a method and method names should start with #
    describe("#listReducer", () => {
        const reducer = wrapReducerForTesting(listReducer);

        // just list the action type
        describe("SAVED", () => {

            describe("when saved with id zero", () => {
                const payload = {
                    id: 0,
                    text: "foo",
                    priority: "low"
                };

                it("should add new item with correct id", () => {
                    expect(reducer(state, {type: "SAVED", payload})).toEqual([...state, {...payload, id: 4}]);
                });
            });

            describe("when saved with specified id", () => {
                const payload = {
                    id: 3,
                    text: "foo",
                    priority: "low"
                };

                it("should edit existing item", () => {
                    expect(reducer(state, {type: "SAVED", payload})).toEqual([state[0], state[1], payload]);
                });
            });
        });

        describe("DELETE", () => {

            it("should return remaining items", () => {
                expect(reducer(state, {type: "DELETE", payload: 2})).toEqual([state[0], state[2]]);
            });
        });

        describe("OTHER_ACTION", () => {

            it("should not change state", () => {
                expect(reducer(state, {type: "OTHER_ACTION"})).toBe(state);
            });
        });
    });

    describe("selectors", () => {

        describe("#getList", () => {

            it("should return list from state", () => {
                expect(getList({list: state})).toBe(state);
            });
        });

        describe("#getListItemById", () => {

            it("should return item from list by id", () => {
                expect(getListItemById({list: state}, 3)).toBe(state[2]);
            });
        });
    });

    describe("#editItemSaga", async () => {
        const dispatch = jest.fn();

        await runSaga({
            dispatch,
            getState: () => ({ list: state }),
        }, editItemSaga, {payload: 1}).done;

        it("should dispatch START_EDITING action with correct list item", () => {
            expect(dispatch).toHaveBeenCalledWith({type: "START_EDITING", payload: state[0]});
        });
    });
});
