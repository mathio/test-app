# test-app

Showcase of software testing.

## The app

Todo list app created via [create-react-app](https://github.com/facebook/create-react-app).

## Run the project

Install dependencies before first run:

```
yarn install
```

Start the project:

```
yarn start
```

## Unit Tests

Run unit-tests via jest:

```
yarn test
```

Run unit-tests via karma and jasmine (in Chrome and Firefox):

```
yarn test-karma
```

## Functional Tests

Functional tests via protractor live in separate directory `./functional-tests` as a separate project.

Run tests directly from this directory:

```
yarn functional-test
```
