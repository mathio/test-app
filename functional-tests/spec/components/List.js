import {isDisplayed} from "../utils/elements";
import {click} from "../utils/actions";
import {Form} from "./Form";


export class List {

    constructor(parent) {
        this._wrapper = parent.$(".todo-list")
    }

    async isDisplayed() {
        return await isDisplayed(this._wrapper);
    }

    get items() {
        return this._wrapper.$$("li");
    }

    async getItemAt(index) {
        return await this.items.get(index);
    }

    async getItemObjects() {
        return await this.items.map(item => ({
            id: item.$(".id").getText(),
            text: item.$(".text").getText(),
        }));
    }

    async editItemAt(index) {
        const item = await this.getItemAt(index);
        await click(item.$("button.edit"));
        return new Form(this._wrapper);
    }

    async deleteItemAt(index) {
        const item = await this.getItemAt(index);
        await click(item.$("button.delete"));
    }
}
