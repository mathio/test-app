import {isDisplayed} from "../utils/elements";
import {List} from "./List";
import {Form} from "./Form";


export class MainPage {

    constructor() {
        this._wrapper = $(".app");
    }

    async open() {
        await browser.get("http://localhost:3000");
        return this;
    }

    async isDisplayed() {
        return await isDisplayed(this._wrapper);
    }

    get list() {
        return new List(this._wrapper);
    }

    get form() {
        return new Form(this._wrapper);
    }

    async addNewItem() {
        await this._wrapper.$("a.add-item").click();
        return this.form;
    }
}
