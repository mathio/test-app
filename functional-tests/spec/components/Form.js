import {isDisplayed} from "../utils/elements";
import {click, getInputValue, setInputValue} from "../utils/actions";


export class Form {

    constructor(parent) {
        this._wrapper = parent.$(".todo-form")
    }

    async isDisplayed() {
        return await isDisplayed(this._wrapper);
    }

    get input() {
        return this._wrapper.$("input");
    }

    async getInputText() {
        return await getInputValue(this.input);
    }

    get selectPriority() {
        return this._wrapper.$("select");
    }

    get buttonSave() {
        return this._wrapper.$("button.save");
    }

    get buttonCancel() {
        return this._wrapper.$("button.cancel");
    }

    async setText(text) {
        await setInputValue(this.input, text);
    }

    async save() {
        await click(this.buttonSave);
    }

    async cancel() {
        await click(this.buttonCancel);
    }
}
