import {MainPage} from "./components/MainPage";

describe("Todo App", () => {
    let page;
    let list;

    beforeAll(async () => {
        page = new MainPage();
        await page.open();
        list = page.list;
    });

    it("should display the todo list", async () => {
        expect(await page.isDisplayed()).toBe(true);
    });

    describe("the todo list", () => {

        it("should be displayed", async () => {
            expect(await list.isDisplayed()).toBe(true);
        });

        it("should display 4 items", async () => {
            expect(await list.items.count()).toBe(4);
        });

        it("should display correct items", async () => {
            expect(await list.getItemObjects()).toEqual([
                {id: "#1", text: "milk"},
                {id: "#2", text: "bread"},
                {id: "#3", text: "eggs"},
                {id: "#4", text: "chocolate"},
            ]);
        });
    });

    describe("when adding new item", () => {
        let form;

        beforeAll(async () => {
            form = await page.addNewItem();
        });

        it("should display form", async () => {
            expect(await form.isDisplayed()).toBe(true);
        });

        describe("when form is filled in and saved", () => {

            beforeAll(async () => {
                await form.setText("foobar");
                await form.save();
            });

            it("should display 5 items in the list", async () => {
                expect(await list.items.count()).toBe(5);
            });

            describe("when 2 items are deleted", () => {
                beforeAll(async () => {
                    await list.deleteItemAt(0);
                    await list.deleteItemAt(0);
                });

                it("should display 3 items in the list", async () => {
                    expect(await list.items.count()).toBe(3);
                });
            });

            describe("when item is edited", () => {
                const indexToEdit = 0;
                let itemText;
                let form;

                beforeAll(async () => {
                    const items = await list.getItemObjects();
                    itemText = items[indexToEdit].text;
                    form = await list.editItemAt(indexToEdit);
                });

                it("should display form", async () => {
                    expect(await form.isDisplayed()).toBe(true);
                });

                it("should display item text in form", async () => {
                    expect(await form.getInputText()).toBe(itemText);
                });

                describe("and text is changed and saved", () => {

                    beforeAll(async () => {
                        await form.setText("hello!");
                        await form.save();
                    });

                    it("the list should display edited item", async () => {
                        const items = await list.getItemObjects();
                        expect(await items[indexToEdit].text).toBe("hello!");
                    });
                });
            });
        });
    });
});
