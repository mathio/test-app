import {ElementFinder, ExpectedConditions as EC} from "protractor";

export const isDisplayed = async (element) => await EC.presenceOf(element)() && await element.isDisplayed();