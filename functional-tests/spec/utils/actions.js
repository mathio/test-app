import {isDisplayed} from "./elements";
import {browser, protractor} from "protractor";

export const scrollIntoView = async (element) => await browser.executeScript(el => el.scrollIntoView(), element.getWebElement());

const waitForElement = async (element, timeout) => await browser.wait(() => isDisplayed(element), timeout, "element to be displayed");

export const clearInputValue = async (element, timeout = 1000) => {
    await waitForElement(element, timeout);
    const currentValue = await element.getAttribute("value");
    for (let i = 0; i < currentValue.length; i += 1) {
        await element.sendKeys(protractor.Key.BACK_SPACE);
    }
};

export const setInputValue = async (input, value, timeout = 1000) => {
    await waitForElement(input, timeout);
    await clearInputValue(input, timeout);
    await input.sendKeys(value);
};

export const getInputValue = async (element, timeout = 1000) => {
    await waitForElement(element, timeout);
    await scrollIntoView(element);
    return await element.getAttribute("value");
};

export const click = async (element, timeout = 1000) => {
    await waitForElement(element, timeout);
    await scrollIntoView(element);
    await browser.wait(() => element.isEnabled(), timeout, "element to be clickable");
    await element.click();
};

export const getText = async (element, timeout = 1000) => {
    await waitForElement(element, timeout);
    await scrollIntoView(element);
    return await element.getText();
};

export const setSelectValue = async (input, value, timeout = 1000) => {
    await waitForElement(input, timeout);
    const option = input.element(by.cssContainingText("option", value));
    await click(option);
};
