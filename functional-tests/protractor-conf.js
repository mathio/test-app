// read more in docs https://github.com/angular/protractor/blob/master/lib/config.ts

exports.config = {

    // seleniumAddress: "http://localhost:4444/wd/hub",
    directConnect: true,    // either address to selenium driver or connect directly to browser (for chrome and firefox)

    specs: [
        "build/**/*.spec.js"
    ],

    onPrepare: function () {
        browser.ignoreSynchronization = true;   // removes "Error while waiting for Protractor to sync with the page"
    },

    stackTrace: true,

    capabilities: {
        browserName: process.env.TEST_BROWSER_NAME || "chrome",
        version: process.env.TEST_BROWSER_VERSION || "ANY",

        chromeOptions: {
            args: []    // for chrome in headless mode: ["--headless", "--disable-gpu", "--window-size=1280,800"]
        }
    },
};